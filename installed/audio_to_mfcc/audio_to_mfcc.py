import librosa
import numpy as np
import math

from ..transform_interface import TransformInterface

# This implementation exactly matches the one in C++ on sensortile in terms of output
class AudioToMFCC(TransformInterface):
    
    def __init__(self, signal_rate, num_frames, n_mfcc, frame_overlap_ratio=0.5, n_fft = None, power = 2, htk = False, norm = 1):
        self.signal_rate = signal_rate
        self.num_frames = num_frames #corresponds to the height of the spectrogram in this case... should probably change
        self.n_mfcc = n_mfcc #width
        # frame_overlap ratio is (frame_len-frame_shift)/frame_len
        # it is an hyper parameter for this function
        self.frame_overlap_ratio = frame_overlap_ratio
        self.n_fft = n_fft
        self.power = power
        self.htk = htk
        self.norm = norm # 0 or 1 


    def __call__(self, signal):
        # frame_len and frame_shift are computed based on desired number of frames and desired overlap
        frame_len = int(len(signal)/(self.num_frames*(1-self.frame_overlap_ratio)+self.frame_overlap_ratio))
        if self.n_fft is None: 
            frame_len_padded = 2**math.ceil(math.log2(frame_len))
            self.n_fft = frame_len_padded
        else: #some kind of validation should be done on n_fft custom? It can't be smaller than frame_len probably
            frame_len_padded = self.n_fft
        frame_shift = math.ceil(frame_len*(1-self.frame_overlap_ratio))
        
        mels_f = librosa.filters.mel(sr=self.signal_rate, n_fft=frame_len_padded, n_mels=40, fmin=20, fmax=4000, norm=None, htk=True)
        window = np.hanning(frame_len+1)[:frame_len]

        features = []

        for nf in range(self.num_frames):
            buffer=np.zeros(frame_len_padded)
            ss = signal[nf*frame_shift:nf*frame_shift+frame_len]
            buffer[:len(ss)] = ss
            buffer[:frame_len] = window*buffer[:frame_len]
            fft=np.fft.rfft(buffer)
            D = np.abs(fft)**self.power
            mels = mels_f.dot(D)
            mels[mels==0] = np.nextafter(np.float32(0), np.float32(1))
            lmels=np.log(mels)
            features.append(librosa.feature.mfcc(S=lmels, sr=self.signal_rate, n_mfcc=self.n_mfcc, dct_type=2, norm='ortho'))

        features = np.array(features)
        # use shape with 3 dimensions
        return np.expand_dims(features, -1)
