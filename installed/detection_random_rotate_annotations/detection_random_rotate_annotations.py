import cv2
import math
import numpy as np
import random
from ..transform_interface import TransformInterface

class DetectionRandomRotateAnnotations(TransformInterface):
    
    def __init__(self,  width = 416, height = 416, max_angle = 15, probability=0.5, sync_code = 0):
        self.probability=probability
        self.sync_code = sync_code
        self.max_angle = max_angle
        self.height = height
        self.width = width

    def __call__(self, y, **kwargs):
      seed = kwargs.get("seed", None)
      if seed is not None:
        seed += self.sync_code
      random.seed(a=seed)
      
      
      if random.random() < self.probability:
        angle = random.uniform(-self.max_angle, self.max_angle)
        res = []
        for yi in y:
          if sum(yi[0:5])>0:
            x1 = yi[0]
            y1 = yi[1]
            w  = yi[2]
            h  = yi[3]
            
            xc=self.width/2
            yc=self.height/2
            
            p=[(x1-xc,y1-yc)] #top left
            p.append((x1+w-xc, y1-yc)) #top right
            p.append((x1+w-xc, y1+h-yc)) #bottom right == x2,y2
            p.append((x1-xc, y1+h-yc)) # bottom left
            
            r = math.radians(angle)
            cos = math.cos(r)
            sin = - math.sin(r) # the images's system of coordinates has the y axe upside down respect the cartesian system
            
           
            p_= [(pp[0]*cos - pp[1]*sin, pp[1]*cos + pp[0]*sin) for pp in p] # compute the new coordinates of the boxes
            
            p_ = np.array(p_)
            
            x1 = p_[:,0].min()+xc # find the coordinates of the new box
            y1 = p_[:,1].min()+yc
            
            x2 = p_[:,0].max()+xc
            y2 = p_[:,1].max()+yc
            
            if x1<0:
              x1=0
            if y1<0:
              y1=0
            if x2<0:
              x2=0
            if y2<0:
              y2=0
              
            if x1>self.width:
              x1=self.width
            if y1>self.height:
              y1=self.height
            if x2>self.width:
              x2=self.width
            if y2>self.height:
              y2=self.height
            
            w = x2-x1
            h = y2-y1
            
            bb = [x1,y1,w,h,yi[4]]

            
            if len(yi)>5:
              bb.append(yi[5])
              bb.append(yi[6]) 
          else:
            bb=[yy for yy in yi]
          
          res.append(bb)

        return res
      return y
        
