
import numpy as np

from ..transform_interface import TransformInterface

class ExpandDims(TransformInterface):
    
    def __init__(self, dims):
      self.dims=dims
      


    def __call__(self, a):
    
        print ("Input shape: ", a.shape)
        d = len(a.shape)
        if d < self.dims:
          for i in range(self.dims-d):
            a = np.expand_dims(a, axis=-1)

        print ("Reshaped input shape: ", a.shape)
        
        
        
        return a
         
