import cv2
import numpy as np

from ..transform_interface import TransformInterface

class PadToSquare(TransformInterface):
    
    def __init__(self, pad_value=128):
        self.pad_value = pad_value


    def __call__(self, img):
        
         h, w, _ = img.shape
         dim_diff = np.abs(h - w)
         pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
         
         # Determine padding
         pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
    
         # Add padding
         img = cv2.copyMakeBorder(img, pad[2], pad[3], pad[0], pad[1], cv2.BORDER_CONSTANT, value=[self.pad_value,self.pad_value,self.pad_value])

         return img

