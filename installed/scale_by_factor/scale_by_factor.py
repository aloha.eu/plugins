import torch

from ..transform_interface import TransformInterface

class ScaleByFactor(TransformInterface):
    
    def __init__(self, factor=1.0):
        self.factor=factor


    def __call__(self, array):
    
        return array*self.factor
         
