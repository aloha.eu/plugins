import cv2
import numpy as np

from ..transform_interface import TransformInterface

class RandomHorizVertShift(TransformInterface):
    
    def __init__(self, pad_value = 128, probability=0.5, maxShift=0.2, horizontal = True, vertical = False, sync_code = 0):
        self.probability = probability
        self.maxShift    = maxShift
        self.horizontal  = horizontal
        self.vertical    = vertical
        self.sync_code   = sync_code
        self.pad_value   = pad_value

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
        random.seed(a=seed)
        
        if random.random() < self.probability:
          
          
          # Store height and width of the image 
          height, width = image.shape[:2] 
          shift_height = 0
          shift_width  = 0
          if self.vertical:
            shift_height = int(height * random.uniform(-self.maxShift, self.maxShift))
          
          if self.horizontal:
            shift_width  = int(width  * random.uniform(-self.maxShift, self.maxShift))
            
          T = np.float32([[1, 0, shift_width], [0, 1, shift_height]]) 
            
          # We use warpAffine to transform 
          # the image using the matrix, T 
          image = cv2.warpAffine(image, T, (width, height))
          
          if self.pad_value <0:
            self.pad_value = int(random.uniform(0, 255))
          pad_ = np.zeros(image.shape) #change padding color
          pad_[0 : width, 0 : shift_height, : ]= self.pad_value
          pad_[0 : shift_width, 0 : height, : ]= self.pad_value
          
          image = image + pad_
          
      return image
