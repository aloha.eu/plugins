import cv2

from ..transform_interface import TransformInterface

class ImageResize(TransformInterface):
    
    def __init__(self, H=32, W=32):
        self.h = H
        self.w = W

    def __call__(self, img):
        
      if img.shape[0] != self.h or img.shape[1] != self.w:
        
        img = cv2.resize(img, (self.w, self.h), interpolation=cv2.INTER_LINEAR)
          
      return img
