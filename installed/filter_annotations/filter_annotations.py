import cv2
import numpy as np

from ..transform_interface import TransformInterface

class FilterAnnotations(TransformInterface):
    
    def __init__(self, minSize = 10):
      self.minSize = minSize


    def __call__(self, y_list, **kwargs):# requires an input with shape [#Objs, 7] <--- x1, y1, w, h, class, imgW, imgH
        w = y_list[0][5]
        h = y_list[0][6]
        
        l=0
        output_list = []
        for bb in y_list:
          if sum(bb[0:5])>0:
            if bb[2] <self.minSize or bb[3] < self.minSize:
              b = [0,
                  0,
                  0,
                  0,
                  0]
              if len(bb)>5:
                b.append(0)
                b.append(0)
            else:
              l += 1
              b = [bb[0],
                   bb[1],
                   bb[2],
                   bb[3],
                   bb[4]]
              if len(bb)>5:
                b.append(bb[5])
                b.append(bb[6])
          output_list.append(b)
          
        if l==0: # if the list is empty after filtering, add at least the biggest box in the image
          area_max=0
          index=0
          for i, bb in enumerate(y_list):
            if area_max<bb[2]* bb[3]:
              area_max=bb[2]* bb[3]
              index =i
          b = [y_list[index][0],
               y_list[index][1],
               y_list[index][2],
               y_list[index][3],
               y_list[index][4]]
          if len(y_list[index])>5:
                b.append(y_list[index][5])
                b.append(y_list[index][6])
          output_list[0]=b
        return output_list

