import cv2
import random
from ..transform_interface import TransformInterface
import numpy as np
class DetectionHorizVertShiftAnnotations(TransformInterface):
    
    def __init__(self, width = 416, maxShift=0.2, horizontal = True, vertical = False, probability=0.5, sync_code = 0):
        
        self.probability=probability
        self.maxShift    = maxShift
        self.horizontal  = horizontal
        self.vertical    = vertical
        self.sync_code = sync_code
        self.width = width
        self.height = height

    def __call__(self, y, **kwargs): # boxes are x1,y1,w,h,class
      seed = kwargs.get("seed", None)
      if seed is not None:
        seed += self.sync_code
      random.seed(a=seed)
        
      if random.random() < self.probability:
        
        height=self.height
        width = self.width 
        shift_height = 0
        shift_width  = 0
        if self.vertical:
          shift_height = int(height * random.uniform(-self.maxShift, self.maxShift))
          
        if self.horizontal:
          shift_width  = int(width  * random.uniform(-self.maxShift, self.maxShift))
            
        
        res = []
        for yi in y:
          yi = np.array(yi).copy()
          if sum(yi[0:5])>0:
            yi[0] = yi[0] + shift_width
            yi[1] = yi[1] + shift_height
          
            yi = self.clip_box(yi)
          
          yi = yi.tolist()
          res.append(yi)
            
        return res
      return y
      
      
    
    def clip_box(self, box):
      
      diffW = 0
      if box[0]<0:
        diffW= -box[0]
        box[0]=0
      
      
      diffH = 0
      if box[1]<0:
        diffH= -box[1]
        box[1]=0
      
      if box[2]+box[0]>self.width:
        box[2]=self.width-box[0]
      box[2] -= diffW
      
      if box[3]+box[1]>self.height:
        box[3]=self.height-box[1]
      box[3] -= diffH
      
      return box
