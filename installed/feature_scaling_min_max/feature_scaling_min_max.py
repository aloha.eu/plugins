from sklearn.preprocessing import MinMaxScaler

from ..transform_interface import TransformInterface

class FeatureScalingMinMax(TransformInterface):

    def __init__(self, config_parameters):
        pass

    def __call__(self, dset_x):
        # Feature scaling using min-max on whole dataset
        print("Perform min-max on whole dataset")
        min_max_scaler = MinMaxScaler(feature_range=(0, 255), copy=True)

        shape_t = dset_x.shape

        min_max_scaler.fit(dset_x.reshape((shape_t[0]*shape_t[1], shape_t[2]* shape_t[3])))
        
        print("data min", min_max_scaler.data_min_)
        print("data max", min_max_scaler.data_max_)
        
        f = open("/opt/www/feature_scaling_test.txt", "a")
        f.write(str(min_max_scaler.data_min_))
        f.write(str(min_max_scaler.data_max_))
        f.close()

        dset_x= min_max_scaler.transform(dset_x.reshape((shape_t[0]*shape_t[1], shape_t[2]*shape_t[3]))).reshape(shape_t)

        return dset_x
