import cv2
import random
from ..transform_interface import TransformInterface
import numpy as np
class DetectionHorizflipAnnotations(TransformInterface):
    
    def __init__(self, width = 416, probability=0.5, sync_code = 0):
        
        self.probability=probability
        self.sync_code = sync_code
        self.width = width

    def __call__(self, y, **kwargs): # boxes are x1,y1,w,h,class
      seed = kwargs.get("seed", None)
      if seed is not None:
        seed += self.sync_code
      random.seed(a=seed)
        
      if random.random() < self.probability:
        res = []
        for yi in y:
          yi = np.array(yi).copy()
          if sum(yi[0:5])>0:
            yi[0] = self.width - yi[0] - yi[2]
          
          
          yi = yi.tolist()
          res.append(yi)
            
        return res
      return y
