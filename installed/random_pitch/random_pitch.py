import librosa
import random
from ..transform_interface import TransformInterface


class RandomPitch(TransformInterface):

    def __init__(self, probability=0.5, sampling_rate=16000, min_n_steps=-2, max_n_steps=2, bins_per_octave=12):
        self.probability=probability
        self.sampling_rate=sampling_rate
        self.min_n_steps=min_n_steps
        self.max_n_steps=max_n_steps
        self.bins_per_octave=bins_per_octave

    def __call__(self,signal):
        if random.random() < self.probability:
            n_steps = random.randint(self.min_n_steps, self.max_n_steps)
            y_shifted=librosa.effects.pitch_shift(signal, self.sampling_rate, n_steps=n_steps, bins_per_octave=self.bins_per_octave)
            return y_shifted
        else:
            return signal
