import random
from ..transform_interface import TransformInterface
from skimage.transform import rescale

import numpy as np

class RandomImageScale(TransformInterface):

    def __init__(self, pad_value = 128, probability=0.5, max_scale_perc = 0.5, sync_code = 0):
        self.pad_value = pad_value
        self.probability=probability
        self.max_scale_perc = max_scale_perc;
        self.sync_code = sync_code;

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
        random.seed(a=seed)
        
        if random.random() < self.probability:
        
            img_size = image.shape[0]
            imagedtype = image.dtype

            scale = random.uniform(1.0 - self.max_scale_perc, 1.0 + self.max_scale_perc)

            image = rescale(
                image,
                (scale, scale),
                multichannel=True,
                preserve_range=True,
                mode="constant",
                anti_aliasing=False,
            )

            if scale < 1.0: # pad remaining part
                diff = (img_size - image.shape[0]) / 2.0
                padding = ((int(np.floor(diff)), int(np.ceil(diff))),) * 2 + ((0, 0),)
                if self.pad_value <0:
                  self.pad_value = int(random.uniform(0, 255))
                image = np.pad(image, padding, mode="constant", constant_values=self.pad_value)

            else: # crop exceeding part
                x_min = (image.shape[0] - img_size) // 2
                x_max = x_min + img_size
                image = image[x_min:x_max, x_min:x_max, ...]
            
            image = image.round()
            image = image.astype(imagedtype)
            
            
        return image
        
        
        
        


