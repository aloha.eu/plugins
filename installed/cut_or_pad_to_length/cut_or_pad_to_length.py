import librosa
import numpy as np

from ..transform_interface import TransformInterface

class CutOrPadToLength(TransformInterface):
    
    def __init__(self, target_length):
        self.target_length = target_length


    def __call__(self, audio_in):
        if len(audio_in) < self.target_length:
            signal = librosa.util.pad_center(audio_in, self.target_length)
        elif len(audio_in) > self.target_length:
            signal = librosa.util.fix_length(audio_in, self.target_length)
        else:
            signal = audio_in

        return signal
