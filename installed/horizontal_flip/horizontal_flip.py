import random
from ..transform_interface import TransformInterface

import numpy as np

class HorizontalFlip(TransformInterface):

    def __init__(self, probability=0.5, sync_code = 0):
        self.probability=probability
        self.sync_code = sync_code;

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
          
        random.seed(a=seed)
        if random.random() < self.probability:
            image = np.fliplr(image).copy()
        
        return image
