import torch

from ..transform_interface import TransformInterface

class TorchFromNumpy(TransformInterface):
    
    def __init__(self):
      pass


    def __call__(self, array):
    
        t = torch.from_numpy(array)
        
        
        # convert from HWC to CHW if 4D tensor and from WC to CW if 3D tensor
        if t.dim()==3:
          t = t.permute(2,0,1)
          
        if t.dim()==2:
          t = t.permute(1,0)
        
        return t
         
