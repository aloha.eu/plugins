import cv2
import random
import numpy as np
from ..transform_interface import TransformInterface

def bbox_area(bbox):
    return (bbox[:,2] - bbox[:,0])*(bbox[:,3] - bbox[:,1])


    
        
        
class DetectionRandomScaleAnnotations(TransformInterface):
    
    def __init__(self, width = 416, height = 416, probability=0.5, max_scale_perc = 0.5, sync_code = 0):
        self.probability=probability
        self.max_scale_perc = max_scale_perc
        self.sync_code = sync_code
        self.height = height
        self.width = width

    def __call__(self, y, **kwargs):
      seed = kwargs.get("seed", None)
      if seed is not None:
        seed += self.sync_code
      random.seed(a=seed)
      
      
      if random.random() < self.probability:
        scale = random.uniform(1.0 - self.max_scale_perc, 1.0 + self.max_scale_perc)
        res = []
        for yi in y:
          
          box=np.array(yi).copy()
          if sum(yi[0:5])>0:
            x1 = yi[0]
            y1 = yi[1]
            w  = yi[2]
            h  = yi[3]
            
            diffcX = self.width/2*(scale-1)
            diffcY = self.height/2*(scale-1)

            box[:4] =box[:4] * scale
            box[0] = box[0] - diffcX
            box[1] = box[1] - diffcY
            
            box = self.clip_box(box)


          if box is not None:
            res.append(box.tolist())
        return res
      return y
        
    def clip_box(self, box):
      
      diffW = 0
      if box[0]<0:
        diffW= -box[0]
        box[0]=0
      
      
      diffH = 0
      if box[1]<0:
        diffH= -box[1]
        box[1]=0
      
      if box[2]+box[0]>self.width:
        box[2]=self.width-box[0]
      box[2] -= diffW
      
      if box[3]+box[1]>self.height:
        box[3]=self.height-box[1]
      box[3] -= diffH
      
      return box
        
        
