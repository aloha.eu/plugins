class TransformInterface:
    def __init__(self, config_parameters):
        raise NotImplementedError("The __init__ method from TransformInterface must be implemented!")

    def __call__(self, input_data):
        raise NotImplementedError("The __call__ method from TransformInterface must be implemented!")

