import cv2
import numpy as np

from ..transform_interface import TransformInterface

class PadToSquareAnnotations(TransformInterface):
    
    def __init__(self):
      pass


    def __call__(self, y_list, **kwargs):# requires an input with shape [#Objs, 7] <--- x1, y1, w, h, class, imgW, imgH
      
        w = y_list[0][5]
        h = y_list[0][6]
        
        w_offset = 0
        h_offset = 0
        if h>w:
          w_offset = (h - w)/2
        else:          
          h_offset = (w - h)/2
        
        output_list = []
        for bb in y_list:
          if sum(bb[0:5])>0:
            b = [ bb[0] + w_offset,
                  bb[1] + h_offset,
                  bb[2],
                  bb[3],
                  bb[4]]
            if len(bb)>5:
              b.append(bb[5])
              b.append(bb[6])
          else:
            b = [0,
                 0,
                 0,
                 0,
                 0]
            if len(bb)>5:
              b.append(0)
              b.append(0)
              
          output_list.append(b)
                
        return output_list

