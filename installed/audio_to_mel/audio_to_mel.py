import librosa
import numpy as np

from ..transform_interface import TransformInterface

class AudioToMel(TransformInterface):

    def __init__(self, signal_rate, n_mels, num_frames):
        self.sr = signal_rate
        self.n_mels = n_mels
        self.num_frames = num_frames

    def __call__(self, signal):
        n_fft = 4096
        nsamples = signal.shape[0]
        hop_length = int(nsamples / (self.num_frames - 1))
        mels = librosa.feature.melspectrogram(signal, sr=self.sr, n_fft=n_fft, n_mels=self.n_mels, hop_length=hop_length)
        mels = np.log(mels + 1e-9)

        return np.expand_dims(mels, -1)

