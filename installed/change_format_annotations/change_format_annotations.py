import cv2
import numpy as np

from ..transform_interface import TransformInterface

class ChangeFormatAnnotations(TransformInterface):
    
    def __init__(self, conversion="x1y1x2y2c_to_x1y1whc"):
      self.conversion = conversion
      # valid conversions are "x1y1x2y2c_to_x1y1whc", "x1y1whc_to_x1y1x2y2c"


    def __call__(self, y_list, **kwargs):# requires an input with shape [#Objs, 6] <--- x1, y1, w, h, class, imgW, imgH
        
        output_list =[]
        for bb in y_list:
          if self.conversion == "x1y1x2y2c_to_x1y1whc":
            b =[ bb[0], #x
                 bb[1], #y1
                 bb[2] - bb[0], #w
                 bb[3] - bb[1], #h
                 bb[4]]
            if len(bb)>5:
              b.append(bb[5])
              b.append(bb[6])
          elif self.conversion == "x1y1whc_to_x1y1x2y2c":
            b =[ bb[0], #x1
                 bb[1], #y1
                 bb[0] + bb[2], #x2
                 bb[1] + bb[3], #y2
                 bb[4]]
            if len(bb)>5:
              b.append(bb[5])
              b.append(bb[6])
          else:
            b =[ bb[0],
                 bb[1],
                 bb[2],
                 bb[3],
                 bb[4]]
            if len(bb)>5:
              b.append(bb[5])
              b.append(bb[6])
          output_list.append(b)
       # print ('output_list')
      #  print (output_list)
        return output_list

