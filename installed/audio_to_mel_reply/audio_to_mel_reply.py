import librosa
import numpy as np
import math

from ..transform_interface import TransformInterface

class AudioToMelReply(TransformInterface):

    def __init__(self, signal_rate, n_mels, num_frames, frame_overlap_ratio = 0, n_fft = None, power = 2, htk = True, norm = np.inf):
        self.sr = signal_rate
        self.n_mels = n_mels
        self.num_frames = num_frames
        self.frame_overlap_ratio = frame_overlap_ratio
        self.n_fft = n_fft
        self.power = power
        self.htk = htk
        self.norm = norm # 0 or 1 

        #TODO configurable data type
        #TODO signal len as a parameter to avoid recomputing mels_f and window every time

        

    def __call__(self, signal):
        # frame_len and frame_shift are computed based on desired number of frames and desired overlap
        frame_len = int(len(signal)/(self.num_frames*(1-self.frame_overlap_ratio)+self.frame_overlap_ratio))
        if self.n_fft is None: 
            frame_len_padded = 2**math.ceil(math.log2(frame_len))
            self.n_fft = frame_len_padded
        else: #some kind of validation should be done on n_fft custom? It can't be smaller than frame_len probably
            frame_len_padded = self.n_fft
        frame_shift = math.ceil(frame_len*(1-self.frame_overlap_ratio))

        mels_f = librosa.filters.mel(sr=self.sr, n_fft=self.n_fft, n_mels=self.n_mels, fmin=20, fmax=4000, norm=self.norm, htk=self.htk)
        window = np.hanning(frame_len+1)[:frame_len]

        features = []
        
        for nf in range(self.num_frames):
            buffer = np.zeros(frame_len_padded)
            right_limit = min(nf*frame_shift+frame_len, len(signal))
            ss = signal[nf*frame_shift:right_limit]
            buffer[:len(ss)] = ss
            buffer[:frame_len] = window*buffer[:frame_len]
            fft = np.fft.rfft(buffer)
            D = np.abs(fft)**self.power
            mels = np.dot(mels_f, D)
            mels[mels == 0] = np.nextafter(np.float32(0), np.float32(1))
            mels=np.log(mels)
            features.append(mels)

        features = np.array(features)
        # use shape with 3 dimensions
        return np.expand_dims(features, -1)
