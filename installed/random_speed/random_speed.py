import librosa
import random
import numpy as np
from ..transform_interface import TransformInterface


#randomly change signal speed
class RandomSpeed(TransformInterface):

    def __init__(self, probability=0.5, min_speed_coef=0.9, max_speed_coef=1.1, top_db=30): 
        self.probability=probability
        self.min_speed_coef=min_speed_coef
        self.max_speed_coef=max_speed_coef
        self.top_db = top_db


    def __call__(self, signal):
        if random.random() < self.probability:
            # Trim leading and trailing silence from an audio signal
            trimmed, _ = librosa.effects.trim(signal, top_db=self.top_db)
            #limit speed up to what is possible without go over original signal lenght
            # FIXME this limit is not necessarily the most general behavior, should be paramtrized?
            max_slowdown_coef = len(trimmed)/len(signal)
            speed_change=np.random.uniform(low=max(max_slowdown_coef, self.min_speed_coef), high=self.max_speed_coef)
            signal_speed_changed = librosa.effects.time_stretch(trimmed, speed_change)
            signal_speed_changed = librosa.util.pad_center(signal_speed_changed, len(signal))
            return signal_speed_changed
        else:
            return signal

