import librosa
import numpy as np
import random
import os

from ..transform_interface import TransformInterface

class RandomSumNoise(TransformInterface):
    
    def __init__(self, probability, noise_folder, min_noise_coef=0.2, max_noise_coef=0.7):
        self.probability = probability
        self.min_noise_coef = min_noise_coef
        self.max_noise_coef = max_noise_coef

        #TODO better config.json, limits on parameters

        noise_samples = []
        # load noise samples paths
        for root, dirs, files in os.walk(noise_folder, topdown=False):
            for f in files:
                if f.endswith('.wav'): #TODO works only with wav...
                    path = root + "/" + f
                    noise, _ = librosa.load(path)
                    noise_samples.append(noise)

        self.noise_samples = noise_samples


    def __call__(self, signal):
        if random.random() < self.probability:
            noise_coef = random.uniform(self.min_noise_coef, self.max_noise_coef)
            noise = random.choice(self.noise_samples)
            if len(signal) > len(noise):
                noise = librosa.util.pad_center(noise, len(signal))
            else:
                cut_here = random.randint(0, len(noise)-len(signal))
                noise = noise[cut_here:cut_here+len(signal)]
            signal_sum = noise_coef * (np.max(signal) / max(np.max(noise), 0.1)) * noise + signal
            return signal_sum
        else:
            return signal
