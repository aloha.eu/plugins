import librosa
import numpy as np

from ..transform_interface import TransformInterface

class LoadAudioFromWav(TransformInterface):
    
    def __init__(self, signal_rate, mono=True, example_len=16000):
        self.signal_rate = signal_rate
        self.mono = mono
        self.example_len = example_len

    def __call__(self, audio_file):
        if audio_file == "":
            return np.random.normal(0, 0.01, self.example_len)
        signal, _ = librosa.load(audio_file, mono=self.mono, sr=self.signal_rate)
        return signal

