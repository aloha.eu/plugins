from sklearn.preprocessing import StandardScaler
import numpy as np

from ..transform_interface import TransformInterface

class FeatureScalingStandard(TransformInterface):

    def __init__(self, config_parameters):
        pass

    def __call__(self, dset_x):
        # Feature scaling using min-max on whole dataset
        print("Perform z-score on whole dataset")
        standard_scaler = StandardScaler(copy=True)

        shape_t = dset_x.shape

        standard_scaler.fit(dset_x.reshape((shape_t[0]*shape_t[1]*shape_t[2], shape_t[3])))
        
        print("data mean", standard_scaler.mean_)
        print("data std", standard_scaler.var_)
        
        f = open("/opt/www/feature_scaling_test_z.txt", "a")
        f.write(str(standard_scaler.mean_))
        f.write(str(standard_scaler.var_))
        f.close()

        dset_x = standard_scaler.transform(dset_x.reshape((shape_t[0]*shape_t[1]*shape_t[2],shape_t[3]))).reshape(shape_t)

        dset_x = np.clip(dset_x * (255/2) + (255/2), 0, 255)

        return dset_x
