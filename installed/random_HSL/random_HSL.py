import cv2
import numpy as np

import random
from ..transform_interface import TransformInterface

class RandomHSL(TransformInterface):
    
    def __init__(self, probability=0.5, brightness_max=64, saturation_max=64, hue_max=64):
        self.probability =  probability
        self.brightness_max = brightness_max
        self.saturation_max = saturation_max
        self.hue_max = hue_max



    def __call__(self, img, **kwargs):
        seed = kwargs.get("seed", None)
        random.seed(a=seed)
        
        if random.random() < self.probability:

          brightness_value = int(random.uniform(-self.brightness_max, self.brightness_max))
          saturation_value = int(random.uniform(-self.saturation_max, self.saturation_max))
          hue_value        = int(random.uniform(-self.hue_max, self.hue_max))
        
          img_hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
          h, s, v = cv2.split(img_hsv)
          
          h = hue_value + h
          s = saturation_value + s
          v = brightness_value + v
          #https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html
          h[h > 179] = 179 
          h[h < 0]   = 0
          h = np.asarray(h, dtype=np.uint8)
          
          s[s > 255] = 255
          s[s < 0]   = 0
          s = np.asarray(s, dtype=np.uint8)
          
          v[v > 255] = 255
          v[v < 0]   = 0
          v = np.asarray(v, dtype=np.uint8)
          
          final_hsv = cv2.merge((h, s, v))
          img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2RGB)

        return img
        
        
        
        
        
        
       
