import random
from ..transform_interface import TransformInterface
import math
import numpy as np

class RandomErase(TransformInterface):

    def __init__(self, color = 128, probability=0.5, form_factor_min = 0.5, form_factor_max = 2.0, max_erase_perc = 0.5, sync_code=0):
        self.color = color
        self.probability=probability
        self.form_factor = [form_factor_min, form_factor_max]
        self.max_erase_perc = max_erase_perc
        self.sync_code = sync_code

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
        random.seed(a=seed)
        
        if random.random() < self.probability:
        
            img_size   = image.shape
            #print (img_size)
            imagedtype = image.dtype
            
            area        = random.uniform(0,img_size[0]*img_size[1] * self.max_erase_perc)
            form_factor = random.uniform(self.form_factor[0], self.form_factor[1])
            
            w  = int(form_factor * math.sqrt(area/form_factor))
            h  = int(math.sqrt(area/form_factor))
            x1 = int(random.uniform(0, img_size[0]-w))
            y1 = int(random.uniform(0, img_size[1]-h))
            #print (area,form_factor,x1,y1,w,h)
            mask = np.ones(img_size, dtype=np.bool)
            mask[x1:x1+w, y1:y1+h, :] = False
            
            if self.color == -1:
              col = [int(random.uniform(0, 255)) for i in range (0,3)]
            else:
              c = self.color
              col = [c for i in range (0,3)]
              
            color_map = np.ones(img_size, dtype=imagedtype)
            for i in range(img_size[2]):
              color_map[ : , : , i] = color_map[ : , : , i]*col[i]
            
            
            image_ = image*mask + color_map*~mask
            return image_
            
        return image
        
        
        
        


