import cv2
import numpy as np

from ..transform_interface import TransformInterface

class ResizeAnnotations(TransformInterface):
    
    def __init__(self, W = 416, H = 416):
      self.W = W #output sizes
      self.H = H


    def __call__(self, y_list, **kwargs):# requires an input with shape [#Objs, 6] <--- x1, y1, w, h, class, imgW, imgH
      
        w = y_list[0][5]
        h = y_list[0][6]
        
        if h>0 and w>0:
          if h>w:
            imgheight_rate = self.H/h
            imgwidth_rate  = imgheight_rate
          else:
            imgwidth_rate  = self.W/w
            imgheight_rate = imgwidth_rate
        else:
          imgheight_rate=0
          imgwidth_rate=0
          
        output_list = []
        for bb in y_list:
          
          if sum(bb[0:5])>0:
            b = [bb[0] * imgwidth_rate,
                 bb[1] * imgheight_rate,
                 bb[2] * imgwidth_rate,
                 bb[3] * imgheight_rate,
                 bb[4]]
            if len(bb)>5:
              b.append(bb[5])
              b.append(bb[6])
          else:
            b=[0,0,0,0,0]
            if len(bb)>5:
              b.append(0)
              b.append(0)
          output_list.append(b)
                
        return output_list

