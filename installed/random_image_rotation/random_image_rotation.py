import random
from ..transform_interface import TransformInterface
from skimage.transform import rotate


class RandomImageRotation(TransformInterface):

    def __init__(self, pad_value = 128, probability=0.5, max_angle=15.0, sync_code=0):
        self.probability=probability
        self.max_angle=max_angle
        self.sync_code = sync_code
        self.pad_value = pad_value

    def __call__(self, image, **kwargs):
        seed = kwargs.get("seed", None)
        if seed is not None:
          seed += self.sync_code
        random.seed(a=seed)
        
        if random.random() < self.probability:
            imagedtype=image.dtype
            angle = random.uniform(-self.max_angle, self.max_angle)
            if self.pad_value <0:
              self.pad_value = int(random.uniform(0, 255))
            image = rotate(image, angle, resize=False, preserve_range=True, mode="constant", cval=self.pad_value)
            image = image.round()
            image = image.astype(imagedtype)
        
        return image
