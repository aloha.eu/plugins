import cv2
import os
import numpy as np

from ..transform_interface import TransformInterface

class LoadImageFromFile(TransformInterface):
    
    def __init__(self, grayscale=False):
        self.grayscale = grayscale

    def __call__(self, img_file):
    
        if img_file=='':
          if self.grayscale:
            img = np.zeros((100,100,1), np.uint8)
          else:
            img = np.zeros((100,100,3), np.uint8)
        else:
          if os.path.isfile(img_file):
            if self.grayscale:
              img = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)
              img = np.expand_dims(img, axis=2)
            else:
              img = cv2.imread(img_file)
              img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
          else:
            print ("No image loaded: {}".format(img_file))
            img = None
        
        return img
