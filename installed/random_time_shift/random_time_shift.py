import librosa
import numpy as np
import random

from ..transform_interface import TransformInterface

class RandomTimeShift(TransformInterface):
    
    def __init__(self, probability, top_db=30):
        self.probability = probability
        self.top_db = top_db


    def __call__(self, signal):
        if random.random() < self.probability:
            # Trim leading and trailing silence from an audio signal
            trimmed, _ = librosa.effects.trim(signal, top_db=self.top_db)
            max_time_shift = len(signal) - len(trimmed)
            time_shift = random.randint(0, max_time_shift)

            signal_time_shifted = np.pad(trimmed, (time_shift, max_time_shift - time_shift), 'constant', constant_values=0)

            return signal_time_shifted
        else:
            return signal
