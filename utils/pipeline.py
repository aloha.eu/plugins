import importlib
import inspect

class Pipeline:
    def __init__(self, dictionary):
        self.pipeline_dict = dictionary
        self.modules_objects = {"validation":{}, "training": {}}

    def _get_column_config(self, column_name, validation=False):
        plugins = list(filter(lambda dict : dict["column"]==column_name ,self.pipeline_dict["config"]))[0]["plugins"]
        if validation==True:
            return list(filter(lambda dict : dict["validation"]==True, plugins))
        return plugins

    def get_transforms(self, column_name, validation=False):
        if validation:
            train_val_key = "validation"
        else:
            train_val_key = "training"

        if column_name in self.modules_objects[train_val_key]:
            return self.modules_objects[train_val_key][column_name]
        else:
            mod_objects = []
            modules_dictionaries = self._get_column_config(column_name, validation)
            if modules_dictionaries:
                print("[loadModules] Starting import of "+str(len(modules_dictionaries))+" modules")
                count = 0
                for plugin_dict in modules_dictionaries:
                    try:
                        imported_module = importlib.import_module(plugin_dict["module"])
                        class_found = False
                        for name, obj in inspect.getmembers(imported_module):
                            if name == plugin_dict["class"] and inspect.isclass(obj) and not class_found:
                                class_found = True
                                module_class = obj
                                print("[loadModules][{}] Init {}".format(count, name))
                                mod_objects.append(module_class(**plugin_dict["parameters"]))
                                break # take the first class implementing the interface
                        if not class_found:
                            raise NotImplementedError(
                                "No class '{}' in module '{}'".format(plugin_dict["class"], plugin_dict["module"]))
                    except ImportError:
                        raise ValueError("Plugin not found! This should cause a failure.")  #FIXME Probably not the most appropriate error to raise?
                    count = count + 1
                print("[loadModules] {} modules loaded".format(count))
                self.modules_objects[train_val_key][column_name] = mod_objects
            else:
                print("[loadModules] No modules specified.")
            return mod_objects
            
            
class Compose:

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, img, **kwargs):
        for t in self.transforms:
            try:
              img = t(img, **kwargs)
            except TypeError:
              img = t(img)
        return img

    def __repr__(self):
        format_string = self.__class__.__name__ + '('
        for t in self.transforms:
            format_string += '\n'
            format_string += '    {0}'.format(t)
        format_string += '\n)'
        return format_string


