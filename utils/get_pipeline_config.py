def get_column_config(pipeline_dict, column_name, validation=False):
    plugins = list(filter(lambda dict : dict["column"]==column_name ,pipeline_dict["config"]))[0]["plugins"]
    if validation==True:
        return list(filter(lambda dict : dict["validation"]==True, plugins))
    return plugins