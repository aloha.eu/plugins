import importlib
import inspect

def get_configured_transforms(modules_dictionaries):
    modules_objects = []
    if modules_dictionaries:
        print("[loadModules] Starting import of ", len(modules_dictionaries), " modules")
        count = 0
        for plugin_dict in modules_dictionaries:
            try:
                imported_module = importlib.import_module(plugin_dict["module"])
                class_found = False
                for name, obj in inspect.getmembers(imported_module):
                    if name == plugin_dict["class"] and inspect.isclass(obj) and not class_found:
                        class_found = True
                        module_class = obj
                        print("[loadModules][{}] Init {}".format(count, name))
                        modules_objects.append(module_class(**plugin_dict["parameters"]))
                        break # take the first class implementing the interface
                if not class_found:
                    raise NotImplementedError(
                        "No class '{}' in module '{}'".format(plugin_dict["class"], plugin_dict["module"]))
            except ImportError:
                print("Plugin not found! This should cause a failure.")
                raise ValueError  #FIXME Probably not the most appropriate error to raise?
            count = count + 1
        print("[loadModules] {} modules loaded".format(count))
    else:
        print("[loadModules] No modules specified.")
    return modules_objects
