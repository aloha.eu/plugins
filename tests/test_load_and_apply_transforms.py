import unittest
import json
from torchvision.transforms import Compose

import os
import sys
PACKAGE_PARENT = '../..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from plugins.utils.pipeline import Pipeline


class TestLoadAndApplyTransforms(unittest.TestCase):

    def test_kws_data_parser(self):
        with open("kws_pipe_instance.json") as f:
            pre_pipeline_instance = json.load(f)

        pipe = Pipeline(pre_pipeline_instance)

        sample_transforms = pipe.get_transforms('sample_transforms')

        sample_transforms_composed = Compose(sample_transforms)

        sample_pipe_test_out = sample_transforms_composed("0b56bcfe_nohash_0.wav")

        print(sample_pipe_test_out.shape)

        batch_transforms = pipe.get_transforms('batch_transforms')

        batch_transforms_composed = Compose(batch_transforms)

        batch_pipe_test_out = batch_transforms_composed(sample_pipe_test_out)

        print(batch_pipe_test_out)
        

if __name__ == '__main__':
    unittest.main()