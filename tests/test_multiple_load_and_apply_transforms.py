import unittest
import json
from torchvision.transforms import Compose

import os
import sys
PACKAGE_PARENT = '../..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from plugins.utils.pipeline import Pipeline


class TestMultipleLoadAndApplyTransforms(unittest.TestCase):

    def test_kws_data_parser(self):
        with open("kws_pipe_instance.json") as f:
            pre_pipeline_instance = json.load(f)

        pipe = Pipeline(pre_pipeline_instance)

        with open("august_reply6.txt", 'r') as f:
            file_lines = f.readlines()

        sample_transforms = pipe.get_transforms('sample_transforms')
        sample_transforms_composed = Compose(sample_transforms)
        batch_transforms = pipe.get_transforms('batch_transforms')
        batch_transforms_composed = Compose(batch_transforms)

        for line in file_lines:
            path = line.split()[0]
            print(path)
            sample_pipe_test_out = sample_transforms_composed(path)
            #print(sample_pipe_test_out.shape)
            batch_pipe_test_out = batch_transforms_composed(sample_pipe_test_out)
            #print(batch_pipe_test_out)
        

if __name__ == '__main__':
    unittest.main()


# /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/0135f3f2_nohash_0.wav
# /home/lrinelli/20200219_ALOHA/shared_data/usecases/REPLY_KWS/yes/00970ce1_nohash_0.wav