import torch.nn as nn
from ..accuracy_interface import AccuracyInterface

class Percentage(AccuracyInterface):
    def __init__(self):
        pass

    def forward(self, y_pred, y_true, n_classes=None): # FIXME n_classes should not be here
        y_pred_soft = nn.Softmax()(y_pred)
        perc = (y_pred_soft.max(dim=1)[1] == y_true.max(dim=1)[1]).sum()
        return perc