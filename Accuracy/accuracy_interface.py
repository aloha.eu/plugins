class AccuracyInterface:
    def __init__(self):
        raise NotImplementedError("The 'init' method for class Accuracy should be implemented!")

    def forward(self, y_pred, y_true, n_classes=None): # FIXME num_classes should not be here
        raise NotImplementedError("The 'forward' method for class Accuracy should be implemented!")