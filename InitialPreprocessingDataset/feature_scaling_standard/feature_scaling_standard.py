from sklearn.preprocessing import StandardScaler
import numpy as np

from ..initial_preprocessing_dataset_interface import InitialPreprocessingDatasetInterface

class FeatureScalingStandard(InitialPreprocessingDatasetInterface):

    def __init__(self):
        #TODO add parameters
        pass

    def apply(self, dset_x, slice_split):
        # Feature scaling using min-max on whole dataset
        print("Perform z-score on whole dataset")
        standard_scaler = StandardScaler(copy=True)

        shape_t = dset_x[sorted(slice_split[0])].shape
        shape_v = dset_x[sorted(slice_split[1])].shape
        print("SHAPE_TTTTT",shape_t)
        print("SHAPE_VVVVV",shape_v)

        standard_scaler.fit(dset_x[sorted(slice_split[0])].reshape((shape_t[0]*shape_t[1], shape_t[2] * shape_t[3])))
        
        print("data mean", standard_scaler.mean_)
        print("data std", standard_scaler.var_)
        
        f = open("/opt/www/feature_scaling_test_z.txt", "a")
        f.write(str(standard_scaler.mean_))
        f.write(str(standard_scaler.var_))
        f.close()

        dset_x[sorted(slice_split[0])] = standard_scaler.transform(dset_x[sorted(slice_split[0])].reshape((shape_t[0]*shape_t[1], shape_t[2] * shape_t[3]))).reshape(shape_t)
        dset_x[sorted(slice_split[1])] = standard_scaler.transform(dset_x[sorted(slice_split[1])].reshape((shape_v[0]*shape_v[1], shape_v[2] * shape_v[3]))).reshape(shape_v)

        dset_x[sorted(slice_split[0])] = np.clip(dset_x[sorted(slice_split[0])] * (255/2) + (255/2), 0, 1)
        dset_x[sorted(slice_split[1])] = np.clip(dset_x[sorted(slice_split[1])] * (255/2) + (255/2), 0, 1)

        return dset_x
