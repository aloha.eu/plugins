from sklearn.preprocessing import MinMaxScaler

from ..initial_preprocessing_dataset_interface import InitialPreprocessingDatasetInterface

class FeatureScalingMinMax(InitialPreprocessingDatasetInterface):

    def __init__(self, config_parameters):
        pass

    def apply(self, dset_x, slice_split):
        # Feature scaling using min-max on whole dataset
        print("Perform min-max on whole dataset")
        min_max_scaler = MinMaxScaler(feature_range=(0, 255), copy=True)

        shape_t = dset_x[sorted(slice_split[0])].shape
        shape_v = dset_x[sorted(slice_split[1])].shape

        min_max_scaler.fit(dset_x[sorted(slice_split[0])].reshape((shape_t[0]*shape_t[1], shape_t[2]* shape_t[3])))
        
        print("data min",min_max_scaler.data_min_)
        print("data max",min_max_scaler.data_max_)
        
        f = open("/opt/www/feature_scaling_test.txt", "a")
        f.write(str(min_max_scaler.data_min_))
        f.write(str(min_max_scaler.data_max_))
        f.close()

        dset_x[sorted(slice_split[0])] = min_max_scaler.transform(dset_x[sorted(slice_split[0])].reshape((shape_t[0]*shape_t[1], shape_t[2]*shape_t[3]))).reshape(shape_t)
        dset_x[sorted(slice_split[1])] = min_max_scaler.transform(dset_x[sorted(slice_split[1])].reshape((shape_v[0]*shape_v[1], shape_v[2]*shape_v[3]))).reshape(shape_v)

        return dset_x
