class InitialPreprocessingDatasetInterface:
    def __init__(self, config_parameters):
        raise NotImplementedError("The __init__ for class InitialPreprocessingDataset must be implemented!")

    def apply(self, dset_x, slice_split):
        raise NotImplementedError("The 'apply' method for class InitialPreprocessingDataset must be implemented!")

