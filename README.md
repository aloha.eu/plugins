# Plugins

This repository contains plugins for the ALOHA tool flow. Plugins can be used to compose preprocessing pipelines to support existing use cases and also new ones in ALOHA experiments.

To support plugins changes have been made to most of the major components of the toolflow, these changes have not yet been merged with the master branch of all these components. To try them out follow the very few and simple instructions in the section *Installation* below.

## Feedback
If you have any suggestion on how this can be done better do not hesitate to open an issue!

## Features

- [x] Custom sample by sample preprocessing during hdf5 dump (DataParser, before training starts, dataset creation)
- [x] Custom entire dataset preprocessing working on complete hdf5 file
- [x] Custom transforms when getting batches in training and validation thorugh plugins (BatchIterator)
- [x] A ordered list of plugins can be used, every plugin uses as input the output of the previous one
- [x] Load user configuration parameters for plugins from db
- [x] Store preprocessing pipelines in db
- [x] GUI integration (new project menu)
- [ ] Dynamic input/output/parameters from previous to next plugin
- [ ] Custom metrics (loss and accuracy functions) usign plugins
- [ ] Parametrizable dataset splitting using plugins
- [ ] Custom dataset file/folder parsing
- [ ] More to come?

## TODO

- [x] Migration to training_engine_v2.0
- [ ] **[WIP]** Support for other task types, __for now it supports only classification__
- [x] GUI integration
- [x] Enforce the use of a specific class inside a specific type of plugin?
- [x] Better failure in training_engine, as of now an error in loading a plugin do not cause a critical error.
- [ ] Split criterion online?
- [x] Data augmentation online


## Installation

Plugins work with the newest version of the Training engine (v2.0), so in the main directory of the ALOHA installation we need to select the appropriate branch to work with it

- `git checkout new_TE`

- `git pull origin new_TE`

Then clone this repository in `shared_data/plugins`, from the root of the ALOHA installation

```cd shared_data; git clone git@gitlab.com:aloha.eu/plugins.git```

**If you already had the `shared_data/plugins` directory do a `git checkout master; git pull origin master` in it to be sure to have the latest changes.**

Then we need to select the appropiate branch for all the tools supporting plugins right now:
* checkout the branch `plugins` for the `training_engine_v2.0`, if you don't have the Training Engine v2.0 you need to clone it (`git clone git@gitlab.com:aloha.eu/training_engine_v2.0.git` in the root of the ALOHA installation)
  - `git checkout plugins`
  - `git pull origin plugins`
* in a similar fashion checkout the branch `plugins` for `dse_engine`
* checkout the branch `plugins` for `orchestrator-backend-api`
* checkout the branch `plugins` for `orchestrator-frontend`

All set, now we just need to build ALOHA, so in the root of the ALOHA install

```sudo ./aloha.sh build <services_file>```

and the in the `orchestrator-frontend`

```sudo npm install```

Now you are ready to start using ALOHA with plugins!

## How to use

The use of preprocessing pipelines is now integrated in the GUI, just create a new preprocessing pipeline from the "New project" menu when setting up a new ALOHA experiment.

## Structure

All usable plugins are currently located in the `installed` folder of this repository, the `orchestrator-backend-api` has a "discovery" routine that monitor this directory and finds plugins to make them available in the GUI.

For each plugin a sub folder should be created inside the `installed` folder. Each plugin must have a `config.json` file to be discovered and correctly used by the toolflow. The python code for the plugin should have a class that inherits the corresponding interface for its type, interfaces are in the `installed` directory.

See the available examples for a better idea of how it works.

