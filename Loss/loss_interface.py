class LossInterface:
    def __init__(self):
        raise NotImplementedError("The 'init' method for class Loss should be implemented!")

    def forward(self, input, target, num_classes=None): # FIXME num_classes should not be here
        raise NotImplementedError("The 'forward' method for class Loss should be implemented!")

