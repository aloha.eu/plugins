from torch.nn import CrossEntropyLoss

from ..loss_interface import LossInterface

class Softmax(LossInterface, CrossEntropyLoss):
    def __init__(self):
        CrossEntropyLoss.__init__(self)

    def forward(self, input, target, num_classes=None): # FIXME num_classes should not be here
        #FIXME cross entropy loss and softmax are not the same thing
        target = target.max(dim=1)[1]
        return CrossEntropyLoss.forward(self, input, target)
